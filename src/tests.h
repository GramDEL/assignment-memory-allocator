#ifndef _TESTS_H_
#define _TESTS_H_
#include "mem.h"
#include "mem_internals.h"

void test1(struct block_header *first_block);
void test2(struct block_header *first_block);
void test3(struct block_header *first_block);
void test4(struct block_header *first_block);
void test5(struct block_header *first_block);
void run_tests();

#endif
