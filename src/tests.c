#include "tests.h"
#include "mem_debug.h"
#include "util.h"
#define HEAP_SIZE 10000

static struct block_header* get_block_from_data(void* data) {
    return (struct block_header*) ((uint8_t*) data - offsetof(struct block_header, contents));
}

void test1(struct block_header *first_block) {
    debug("Тест 1: однократное выделение и очистка памяти\n");
    void *data = _malloc(HEAP_SIZE/10);
    if (data == NULL) {
        err("Ошибка: malloc вернул null");
    }
    debug_heap(stdout, first_block);
    if (first_block->is_free != false || first_block->capacity.bytes != HEAP_SIZE/10) {
        err("Ошибка: is_free или capacity выставились некорректно");
    }
    _free(data);
    debug_heap(stdout, first_block);
    debug("Тест 1 пройден успешно!\n\n");
}

void test2(struct block_header *first_block) {
    debug("Тест 2: выделение памяти дважды, очистка одного из блоков\n");
    void *data1 = _malloc(HEAP_SIZE/10);
    void *data2 = _malloc(HEAP_SIZE/10);
    if (data1 == NULL || data2 == NULL) {
        err("Ошибка: malloc вернул null");
    }
    debug_heap(stdout, first_block);
    _free(data2);
    debug_heap(stdout, first_block);

    struct block_header *data1_block = get_block_from_data(data1);
    struct block_header *data2_block = get_block_from_data(data2);
    if (data2_block->is_free == false) {
        err("Ошибка: блок 2 не очищен");
    }
    if (data1_block->is_free == true) {
        err("Ошибка: блок 1 очищен (а не должен быть)");
    }
    debug("Tест 2 пройден успешно!\n\n");
    _free(data1);
}

void test3 (struct block_header *first_block) {
    debug("Тест 3: выделение памяти трижды, удаление дважды\n");
    void *data1 = _malloc(HEAP_SIZE/10);
    void *data2 = _malloc(HEAP_SIZE/10);
    void *data3 = _malloc(HEAP_SIZE/10);
    if (data1 == NULL || data2 == NULL || data3 == NULL) {
        err("Ошибка: malloc вернул null");
    }
    debug_heap(stdout, first_block);
    _free(data2);
    debug_heap(stdout, first_block);
    _free(data3);
    debug_heap(stdout, first_block);

    struct block_header *data1_header = get_block_from_data(data1);
    struct block_header *data2_header = get_block_from_data(data2);
    struct block_header *data3_header = get_block_from_data(data3);
    if (data2_header->is_free == false) {
        err("Ошибка: блок 2 не очищен");
    }
    if (data3_header->is_free == false) {
        err("Ошибка: блок 3 не очищен");
    }
    if (data1_header->is_free == true) {
        err("Ошибка: блок 1 очищен (а не должен быть)");
    }
    debug("Тест 3 пройден успешно!\n\n");
    _free(data1);
}

void test4(struct block_header *first_block) {
    debug("Тест 4: изначального места в куче недостаточно, но можно расширить\n");
    void *data1 = _malloc(HEAP_SIZE);
    void *data2 = _malloc(HEAP_SIZE);
    if (data1 == NULL || data2 == NULL) {
        err("Ошибка: malloc вернул null");
    }
    debug_heap(stdout, first_block);

    struct block_header *data1_block = get_block_from_data(data1);
    struct block_header *data2_block = get_block_from_data(data2);
    if ((uint8_t*) data1_block->contents + data1_block->capacity.bytes != (uint8_t*) data2_block){
        err("Ошибка: блок выделен не рядом");
    }
    debug("Тест 4 пройден успешно!\n\n");
    _free(data2);
    _free(data1);
}

void test5(struct block_header *first_block) {
    debug("Тест 5: изначального места в куче недостаточно, расширить нельзя, надо выделять где-то ещё\n");
    void *data1 = _malloc(HEAP_SIZE*3);
    if (data1 == NULL) {
        err("Блок равен NULL");
    }

    struct block_header *start = first_block;
    while (start->next != NULL) start = start->next;
    map_pages((uint8_t *) start + size_from_capacity(start->capacity).bytes, 1000, MAP_FIXED_NOREPLACE);
    void *data2 = _malloc(HEAP_SIZE*30);
    debug_heap(stdout, first_block);

    struct block_header *data2_block = get_block_from_data(data2);
    if (data2_block == start) {
        err("Ошибка: блок выделился рядом");
    }
    debug("Тест 5 пройден успешно!\n\n");
    _free(data2);
    _free(data1);
}

void run_tests() {
    debug("Инициализируем кучу с изначальным размером HEAP_SIZE...\n");
    void* heap = heap_init(HEAP_SIZE);

    if (heap == NULL) {
        err("Ошибка: невозможно инициализировать кучу");
    } else {
        debug("Куча успешно инициализирована!\n\n");
        struct block_header* first_block = (struct block_header*) heap;

        test1(first_block);
        debug("Состояние кучи после теста 1:\n");
        debug_heap(stdout, first_block);
        debug("\n");

        test2(first_block);
        debug("Состояние кучи после теста 2:\n");
        debug_heap(stdout, first_block);
        debug("\n");

        test3(first_block);
        debug("Состояние кучи после теста 3:\n");
        debug_heap(stdout, first_block);
        debug("\n");

        test4(first_block);
        debug("Состояние кучи после теста 4:\n");
        debug_heap(stdout, first_block);
        debug("\n");

        test5(first_block);
        debug("Состояние кучи после теста 5:\n");
        debug_heap(stdout, first_block);
        debug("\nВсе тесты успешно пройдены!\n");
    }
}
